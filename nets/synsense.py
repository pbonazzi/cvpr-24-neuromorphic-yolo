import torch.nn as nn

class SynSenseEyeTracking(nn.Module):
    def __init__(self, args):
        super(SynSenseEyeTracking, self).__init__()
        self.S, self.B, self.C = args.SxS_Grid, args.num_boxes, args.num_classes #num_classes-1
        self.seq = nn.Sequential(
            # P1
            nn.Conv2d(in_channels=2, out_channels=16, kernel_size=5, stride=2, padding=1, bias=True),
            nn.ReLU(),
            nn.AvgPool2d(4, 4),

            # P2
            nn.Conv2d(in_channels=16, out_channels=64, kernel_size=3, stride=1, padding=1, bias=True),
            nn.ReLU(),
            #nn.AvgPool2d(2, 2),

            # C2F 3
            nn.Conv2d(in_channels=64, out_channels=16, kernel_size=1, stride=1, padding=1, bias=True),
            nn.ReLU(),
            #nn.AvgPool2d(2, 2),

            # C2F 5
            nn.Conv2d(in_channels=16, out_channels=16, kernel_size=3, stride=1, padding=1, bias=True),
            nn.ReLU(),
            #nn.AvgPool2d(2, 2),

            # SPPF 6
            nn.Conv2d(in_channels=16, out_channels=8, kernel_size=3, stride=1, padding=1, bias=True),
            nn.ReLU(),
            #nn.AvgPool2d(2, 2),

            # SPPF 7
            nn.Conv2d(in_channels=8, out_channels=16, kernel_size=3, stride=1, padding=1, bias=True),
            nn.ReLU(),
            nn.AvgPool2d(4, 4),

            # Dense Layer 8
            nn.Flatten(),
            nn.Linear(256, 128),
            nn.ReLU(),
            
            # Dense Layer 9
            nn.Linear(128, self.S * self.S *(self.C + self.B * 5)),
            #nn.ReLU(),
        )

    def forward(self, x):
        return self.seq(x)
