# - Import statements
import torch
import samna
import time
from tqdm.auto import tqdm
import numpy as np
import torch.nn as nn
from torchvision import datasets
import sinabs
from sinabs.from_torch import from_model
from sinabs.backend.dynapcnn import io
from sinabs.backend.dynapcnn import DynapcnnNetwork
from sinabs.backend.dynapcnn.chip_factory import ChipFactory


# - Define CNN model

ann = nn.Sequential(
    nn.Conv2d(2, 20, 5, 1, bias=False),
    nn.ReLU(),
    nn.AvgPool2d(2, 2),
    nn.Conv2d(20, 32, 5, 1, bias=False),
    nn.ReLU(),
    nn.AvgPool2d(2, 2),
    nn.Conv2d(32, 128, 3, 1, bias=False),
    nn.ReLU(),
    nn.AvgPool2d(2, 2),
    nn.Flatten(),
    nn.Linear(128, 500, bias=False),
    nn.ReLU(),
    nn.Linear(500, 10, bias=False),
)


sinabs_model = from_model(ann, add_spiking_output=True, min_v_mem=-1, batch_size=1)

# - Input dimensions
input_shape = (2, 28, 28)

# - DYNAP-CNN compatible network
dynapcnn_net = DynapcnnNetwork(
    sinabs_model.spiking_model,
    input_shape=input_shape,
    discretize=True,
    dvs_input=False,
)

# Apply model to device such as dynapcnndevkit, speck2, speck2b
device = "dynapcnndevkit:0"
dynapcnn_net.to(device)