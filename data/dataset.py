"""
This is a file script used for training SNN on the inviation dataset
"""


###################################
# import modules
import datetime
import pathlib
from typing import Tuple, List, Union

import cv2
import pdb
import csv
import random
import os

from collections import namedtuple
import numpy as np
import pandas as pd
import dv_processing as dv
from skimage import transform
from torch.utils.data import Dataset
from PIL import Image, ImageDraw
from tqdm import tqdm
import torch
import matplotlib.pyplot as plt

#import lava.lib.dl.slayer as snn
#import sparseconvnet as scn


###################################
# processing of the frames from iniviation

class AedatProcessorBase:
    """
    Base class processing aedat4 files.

    Manages basic bookkeeping which is reused between multiple implementations.
    """

    def __init__(self, path, filter_noise):
        # Aedat4 recording file
        self.path = path
        self.recording = dv.io.MonoCameraRecording(str(path))
        self.lowest_ts, self.highest_ts = self.recording.getTimeRange()

        # Filter chain removing noise
        self.filter_chain = dv.EventFilterChain()
        if filter_noise:
            self.filter_chain.addFilter(dv.RefractoryPeriodFilter(self.recording.getEventResolution(), refractoryPeriod=datetime.timedelta(microseconds=2000)))
            self.filter_chain.addFilter(dv.noise.BackgroundActivityNoiseFilter(self.recording.getEventResolution()))

        # Bookkeeping
        self.current_ts = self.lowest_ts

    def get_recording_time_range(self):
        """Get the time range of the aedat4 file recording."""
        return self.lowest_ts, self.highest_ts

    def get_current_ts(self):
        """Get the most recent readout timestamp."""
        return self.current_ts

    def __read_raw_events_until(self, timestamp):
        assert timestamp >= self.current_ts
        assert timestamp >= self.lowest_ts
        assert timestamp <= self.highest_ts

        events = self.recording.getEventsTimeRange(int(self.current_ts), int(timestamp))
        self.current_ts = timestamp

        return events

    def read_events_until(self, timestamp):
        """Read event from aedat4 file until the given timestamp."""
        events = self.__read_raw_events_until(timestamp)
        self.filter_chain.accept(events)
        return self.filter_chain.generateEvents()

    def generate_frame(self, timestamp):
        """Generate an image frame at the given timestamp."""
        raise NotImplementedError


class AedatProcessorLinear(AedatProcessorBase):
    """Aedat file processor using accumulator with linear decay."""

    def __init__(self, path, contribution, decay, neutral_val, ignore_polarity=False, filter_noise=False):
        """
        Constructor.

        :param path: path to an aedat4 file to read
        :param contribution: event contribution
        :param decay: accumulator decay (linear)
        :param neutral_val:
        :param ignore_polarity:
        :param filter_noise: if true, noise pixels will be filtered out
        """
        super().__init__(path, filter_noise)

        # Accumulator drawing the events on images
        self.accumulator = dv.Accumulator(self.recording.getEventResolution(),
                                          decayFunction=dv.Accumulator.Decay.LINEAR,
                                          decayParam=decay,
                                          synchronousDecay=True,
                                          eventContribution=contribution,
                                          maxPotential=1.0,
                                          neutralPotential=neutral_val,
                                          minPotential=0.0,
                                          rectifyPolarity=ignore_polarity)

    def collect_events(self, start_timestamp, end_timestamp)-> np.array:
        
        # slice the event array
        events = self.read_events_until(end_timestamp)
        return events.sliceTime(start_timestamp)

    def generate_frame_2d(self, timestamp)-> np.ndarray:
        """
        Generate a 2D frame from events
        """
        events = self.read_events_until(timestamp)
        coord = events.coordinates()
        features = events.polarities().astype(np.byte) 
        image = np.zeros((480, 640, 2))

        # Accumulate events of polarity 0
        polarity_0_indices = np.where(features == 0)
        np.add.at(image, (coord[polarity_0_indices, 1], coord[polarity_0_indices, 0], 0), 1)

        # Accumulate events of polarity 1
        polarity_1_indices = np.where(features == 1)
        np.add.at(image, (coord[polarity_1_indices, 1], coord[polarity_1_indices, 0], 1), 1)

        # Buffer
        image = image.astype("uint8")

        # fig, ax = plt.subplots(1, 2)
        # ax[0].imshow(image[..., 0], cmap='gray')
        # ax[0].set_title('Negative')
        # ax[1].imshow(image[..., 1], cmap='gray')
        # ax[1].set_title('Positive')
        # plt.show()

        # pdb.set_trace()
        assert image.dtype == np.uint8

        return image


    def generate_frame(self, timestamp) -> np.ndarray:
        """
        Generate a 1D frame from events
        """
        events = self.read_events_until(timestamp)
        self.accumulator.accept(events)
        image = self.accumulator.generateFrame().image
        assert image.dtype == np.uint8

        return image


def read_csv(path, is_with_ellipsis, is_with_coords):
    """
    Read a csv file and reatain all columns with the listed column names.
    Depending on the configuation, a different set of columns from the file is retained
    """
    header_items = ['timestamp', 'possible']
    if is_with_coords is True:
        header_items.append('center_x')
        header_items.append('center_y')
    if is_with_ellipsis is True:
        header_items.append('axis_x')
        header_items.append('axis_y')
        header_items.append('angle')

    label_file_df = pd.read_csv(path)
    label_file_df = label_file_df[header_items]

    return label_file_df



###################################
# preparation of the dataset

class BronzeDataset(Dataset):
    'Initialize by creating a time ordered stack of frames and events'
    def __init__(self, base_path: str, train:bool=True, generate_video:bool=True):

        # os walk thorugh the files
        self.base_path = base_path
        experiments = os.listdir(base_path)
        experiments.remove("labels")
        experiments.remove("silver.csv")
        experiments.remove("images")
        experiments.sort()

        self.y = []
        max_label_x, max_label_y = 0, 0
        for i, exp in tqdm(enumerate(experiments), desc="Loading Dataset Paths..."):
            labels_path = pathlib.Path(os.path.join(base_path, exp, "annotations.csv"))
            labels = read_csv(labels_path, False, True)
            labels = labels.sort_values(by="timestamp")

            skip_cnt = 123
            t_start = 0

            for label in labels.itertuples():
                if skip_cnt > 0:
                    t_start = label.timestamp
                    skip_cnt -= 1
                    continue

                center_x = int(label.center_x) # max x = 565 -> 640
                center_y = int(label.center_y) # max y = 386 -> 480

                self.y.append({"exp": exp, "t_start": t_start, "t_end": label.timestamp, "x_coord": center_x, "y_coord": center_y})
                t_start = label.timestamp
           
    def generate_video(self):
        if generate_video: 
            out = cv2.VideoWriter(filename=f'output/{exp}.avi', fourcc=cv2.VideoWriter_fourcc(*'PIM1'), fps=50, frameSize =(640,480), isColor=True)
            img = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
            img = cv2.circle(img, (center_x, center_y), radius=10, color=(255, 0, 0), thickness=-1)
            out.write(img)
            out.release()
    
    def compute_statistics(self)-> dict:

        # Compute the statistics of the dataset
        statistics = {
        "time_length": [], 
        "sampling_time": [],
        "max_img_intensity":0,
        "events_per_sample": [],}

        for index in tqdm(range(self.__len__())):

            # events
            label = self.y[index]
            aedat_path = pathlib.Path(os.path.join(self.base_path, label["exp"], "events.aedat4"))
            aedat_processor = AedatProcessorLinear(aedat_path, 0.25, 1e-7, 0.5)
            
            # img = aedat_processor.generate_frame(label["t_end"])
            # statistics["max_img_intensity"] = max(statistics["max_img_intensity"], img.max())
            # events = aedat_processor.collect_events(label["t_start"], label["t_end"])
            # timestamps = events.timestamps()

            # #### time length of every sample
            # statistics["time_length"].append(timestamps[-1] - timestamps[0])

            # #### sampling time
            # unique, count = np.unique(timestamps, return_counts=True)
            # start_ = np.append([0], unique)
            # end_ = np.append(unique, [0])
            # delta = (end_ - start_)[1:-1]
            # statistics["sampling_time"].append(delta)
            # statistics["events_per_sample"].append(count)


        # sampling time
        # block = np.hstack(statistics["sampling_time"])
        # print("Sampling Time Stats") # Min : 61, Max 346, Mean 200, Std 13, Median 200
        # print("Min : %0d, Max %0d, Mean %0d, Std %0d, Median %0d"%(block.min(), block.max(), block.mean(), block.std(), np.median(block)))
        
        # # # of events
        # block = np.hstack(statistics["events_per_sample"])
        # print("Number of Events Stats") # Min : 3, Max 6255, Mean 175, Std 231, Median 94
        # print("Min : %0d, Max %0d, Mean %0d, Std %0d, Median %0d"%(block.min(), block.max(), block.mean(), block.std(), np.median(block)))
        
        # # # of events
        # block = statistics["time_length"]
        # print("Number of Events Stats") # Min : 19580, Max 4799839, Mean 35492, Std 64210, Median 19864
        # print("Min : %0d, Max %0d, Mean %0d, Std %0d, Median %0d"%(np.min(block), np.max(block), np.mean(block), np.std(block), np.median(block)))
        

        return statistics

    def __getitem__(self, index):

        label = self.y[index]
        aedat_path = pathlib.Path(os.path.join(self.base_path, label["exp"], "events.aedat4"))
        aedat_processor = AedatProcessorLinear(aedat_path, 0.25, 1e-7, 0.5)
        framed_events = aedat_processor.generate_frame(label["t_end"])

        #only events in that specific timeframe
        #events = aedat_processor.collect_events(label["t_start"], label["t_end"])
        #framed_events = aedat_processor.generate_frame_like_events(events, index)
        
        return {"framed_events": framed_events, "label": label}
    
    def __len__(self):
        return len(self.y)


def get_transformation_matrix(dsize: list):

    # Define the range of affine transformation parameters
    angle = np.random.uniform(-90, 90)
    scale = 1
    tx = np.random.uniform(-0.5, 0.5)
    ty = np.random.uniform(-0.5, 0.5)

    # Define the affine transformation matrix
    M = cv2.getRotationMatrix2D((dsize[0]/2, dsize[1]/2), angle, scale)
    M[:, 2] += (tx, ty)

    return M


def augment_data(image, label, M, verbose=False):

    # Apply the affine transformation to the image and the label
    img_transformed = cv2.warpAffine(image, M, (image.shape[1], image.shape[0]), borderMode=cv2.BORDER_REPLICATE)
    label_transformed = tuple(np.round(np.dot(M, np.array((label[0], label[1], 1)))))

    if verbose: 
        # augmented data
        img = Image.fromarray(image)
        draw = ImageDraw.Draw(img)
        radius = 10
        x, y = int(label[0]),  int(label[1])
        draw.ellipse((x-radius, y-radius, x+radius, y+radius), fill=255, outline=0)
        img.save("tests/verbose_data.png")

        # augmented data
        img = Image.fromarray(img_transformed)
        draw = ImageDraw.Draw(img)
        radius = 10
        x, y = int(label_transformed[0]),  int(label_transformed[1])
        draw.ellipse((x-radius, y-radius, x+radius, y+radius), fill=255, outline=0)
        img.save("tests/verbose_augment_data.png")

    return img_transformed, label_transformed

class IniVationDataset(Dataset):
    'Initialize by creating a time ordered stack of frames and events'
    def __init__(self, base_path: str, args: object, device:torch.device, list_experiments: list, train:bool=True):
        
        # Yolo Splits
        self.S = args.SxS_Grid
        self.B = args.num_boxes
        self.C = args.num_classes
        self.dsize = args.dsize
        self.device = device

        self.base_path = base_path
        self.y = pd.read_csv(os.path.join(base_path, "silver.csv"))
        experiments = np.unique(self.y["exp_name"]).tolist()

        self.train = train
        filter_values = [experiments[item] for item in list_experiments]
        self.y = self.y[self.y['exp_name'].isin(filter_values)]

    def __len__(self):
        return len(self.y)

    def __getitem__(self, index, separate_polarities=True, augment=True, resize=True, noise=True):

        row = self.y.iloc[index]
        label =  (row["x_coord"], row["y_coord"])
        x, y = label

        if separate_polarities:
            aedat_path = pathlib.Path(os.path.join(self.base_path, row["exp_name"], "events.aedat4"))
            aedat_processor = AedatProcessorLinear(aedat_path, 0.25, 1e-7, 0.5)
            image = aedat_processor.generate_frame_2d(timestamp=row["t_end"])
        else: 
            image = cv2.imread(row[-1], cv2.IMREAD_UNCHANGED)

        # augment
        if augment and self.train:
            M = get_transformation_matrix(dsize=[image.shape[1], image.shape[0]])
            image, label = augment_data(image, label, M)
            x, y = label

        # resize
        if resize: 
            image = cv2.resize(image, dsize=self.dsize)
            x, y  = x //  (640/self.dsize[0]), y // (480/self.dsize[1])
            
        # noise
        if noise and self.train:
            noise = np.random.normal(0, 10, image.shape)
            image = np.clip(image + noise, 0, 255)

        # norm
        h, w, c = image.shape
        image = torch.tensor(image/255, dtype=torch.float).to(self.device)
        x_delta, y_delta = 10/w, 10/h
        x_norm, y_norm = x/w, y/h
        x_1, y_1 = x_norm-x_delta,y_norm-y_delta
        x_2, y_2 = x_norm+x_delta,y_norm+y_delta

        if x_norm >= 1 or y_norm >= 1:
            # print(f"x_norm : {x_norm}, y_norm : {y_norm}")
            if x_norm >= 1:
                x_norm = float(1) - 1e-6
            if y_norm >= 1:
                y_norm = float(1) - 1e-6

        
        # grid
        label_matrix = torch.zeros((self.S, self.S, self.C +  5 * self.B), dtype=torch.float).to(self.device)
        row, column = int(self.S * y_norm), int(self.S * x_norm)
        
        # label
        label_matrix[row, column, self.C] = 1 # obj conf
        box_coordinates = torch.tensor([x_1, y_1, x_2, y_2])
        label_matrix[row, column, (self.C+1):(self.C+1+4)] = box_coordinates # box coord
        label_matrix[row, column, 0] = 1 # class

        return image, label_matrix


# if __name__ == '__main__':
#     import shutil, configparser
#     #fnum, idnum, bbleft, bbtop, bbwid  bbhei , conf, class, visibility

#     # Example how to generate an image at the given timestamp,
#     base_path = "/home/thor/projects/data/EyeTrackingDataSet_FromInivation"
#     df = pd.read_csv(os.path.join(base_path, 'silver.csv'))

#     out_dir = "/home/thor/projects/yolov8_tracking/val_utils/data/MOTCUSTOM/"
#     paths = os.listdir(base_path)
#     paths.remove("labels")
#     paths.remove("silver.csv")
#     paths.remove("images")

#     for i, p in enumerate(paths):
#         if i==0:
#             out = os.path.join(out_dir, "test")
#         else:
#             out = os.path.join(out_dir, "train")

#         # image folder
#         # img_folder = os.path.join(base_path, p, "images")
#         img_folder_copy = os.path.join(out, "MOTCUSTOM-%03d"%(i+1), "img1")
#         # new_paths=os.listdir(img_folder_copy)
#         # new_paths.sort(reverse=True)
#         # pdb.set_trace()
#         # for j, new_pa in enumerate((new_paths)):
#         #     #os.remove(os.path.join(img_folder, new_pa))
#         #     os.rename(os.path.join(img_folder_copy, new_pa), os.path.join(img_folder_copy, "%06d.jpg"%(len(new_paths)-j)))

#         pdb.set_trace()
#         # try:
#         #     shutil.copytree(img_folder, img_folder_copy)
#         # except Exception as e:
#         #     print(e)

#         # # gt.tx
#         pdb.set_trace()
#         os.makedirs(os.path.join(out, "MOTCUSTOM-%03d"%(i+1), "gt"), exist_ok=True)
#         gt_file = os.path.join(out, "MOTCUSTOM-%03d"%(i+1), "gt", "gt.txt")
#         video_df=df[df["exp_name"]==p]

#         with open(gt_file, "w") as f:
#             # Write some text to the file
#             for k, (index, row) in enumerate(tqdm(video_df.iterrows())): 
#                 frame_number = k+1
#                 identiy_number = 1
#                 bbox_left= max(row["x_coord"]-2, 0)
#                 bbox_top= max(row["y_coord"] -2, 0)
#                 bbox_width= min(4, (-bbox_left + row["x_coord"])*2)
#                 bbox_height=  min(4, (-bbox_top + row["y_coord"])*2)
#                 cong_score= 1
#                 obj_class= 1
#                 visibility = 1
#                 f.write(f"{frame_number} {identiy_number} {bbox_left} {bbox_top} {bbox_width} {bbox_height} {cong_score}  {obj_class}  {visibility} \n")
    
#         # seqfile
#         seqinfo_file = os.path.join(out, "MOTCUSTOM-%03d"%(i+1), "seqinfo.ini")
#         config = configparser.ConfigParser()
#         config['Sequence'] = {"seqLength": len(os.listdir(img_folder_copy)), "imWidth": 640, "imHeight": 480 }

#         with open(seqinfo_file, 'w') as configfile:
#             config.write(configfile)

#         pdb.set_trace()





if __name__ == '__main__':
    import shutil

    # Example how to generate an image at the given timestamp,
    base_path = "/home/thor/projects/data/EyeTrackingDataSet_FromInivation"
    df = pd.read_csv(os.path.join(base_path, 'silver.csv'))

    for index, row in tqdm(df.iterrows()):

        # Set the source and destination paths
        src_path = row["img_path"]

        if row["exp_name"] == "2022-05-17_rokas_01_L":
            dst_path = "/home/thor/projects/data/EyeTrackingDataSet_FromInivation/images/val/%08d.jpg"%index
        else:    
            dst_path = "/home/thor/projects/data/EyeTrackingDataSet_FromInivation/images/train/%08d.jpg"%index


        # Copy the file to the destination folder
        shutil.copy(src_path, dst_path)
        
        # Open the file in write mode
        with open(dst_path.replace(".jpg", ".txt").replace("images", "labels"), "w") as f:
            # Write some text to the file
            x_center = row["x_coord"]/640
            y_center = row["y_coord"]/480
            f.write(f"0 {x_center} {y_center} 0.2 0.2")


# if __name__ == '__main__':
#     # Example how to generate an image at the given timestamp,
#     base_path = "/home/thor/projects/data/EyeTrackingDataSet_FromInivation"
#     dataset = BronzeDataset(base_path)

#     # Column names
#     columns = ['exp_name', 't_start', 't_end', 'x_coord', 'y_coord', 'img_path']

#     # Write the data to a CSV file
#     with open(os.path.join(base_path, 'silver.csv'), 'w', newline='') as csvfile:
#         writer = csv.DictWriter(csvfile, fieldnames=columns)
#         writer.writeheader()

#         for i in tqdm(range(len(dataset))):
    
#             item = dataset[i]
#             img_dir = os.path.join(base_path, item["label"]["exp"], "images")
#             os.makedirs(img_dir, exist_ok=True)
#             path_to_img = os.path.join(img_dir, '%06d.jpg'%i)

#             img = Image.fromarray(item["framed_events"], mode='L')
#             img.save(path_to_img)

#             row = { 'exp_name': item["label"]["exp"], 
#                     't_start': item["label"]["t_start"], 
#                     't_end': item["label"]["t_end"], 
#                     'x_coord': item["label"]["x_coord"], 
#                     'y_coord': item["label"]["y_coord"], 
#                     'img_path': path_to_img}

#             # save image 
#             writer.writerow(row)


    # dataset.compute_statistics()




