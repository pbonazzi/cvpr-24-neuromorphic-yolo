import datetime
import pathlib

import cv2
import numpy as np
import pandas as pd
import dv_processing as dv
import pdb

class AedatProcessorBase:
    """
    Base class processing aedat4 files.

    Manages basic bookkeeping which is reused between multiple implementations.
    """

    def __init__(self, path, filter_noise):
        # Aedat4 recording file
        self.path = path
        self.recording = dv.io.MonoCameraRecording(str(path))
        self.lowest_ts, self.highest_ts = self.recording.getTimeRange()

        # Filter chain removing noise
        self.filter_chain = dv.EventFilterChain()
        if filter_noise:
            self.filter_chain.addFilter(
                dv.RefractoryPeriodFilter(self.recording.getEventResolution(), refractoryPeriod=datetime.timedelta(microseconds=2000)))
            self.filter_chain.addFilter(dv.noise.BackgroundActivityNoiseFilter(self.recording.getEventResolution()))

        # Bookkeeping
        self.current_ts = self.lowest_ts

    def get_recording_time_range(self):
        """Get the time range of the aedat4 file recording."""
        return self.lowest_ts, self.highest_ts

    def get_current_ts(self):
        """Get the most recent readout timestamp."""
        return self.current_ts

    def __read_raw_events_until(self, timestamp):
        assert timestamp >= self.current_ts
        assert timestamp >= self.lowest_ts
        assert timestamp <= self.highest_ts

        events = self.recording.getEventsTimeRange(int(self.current_ts), int(timestamp))
        self.current_ts = timestamp

        return events

    def read_events_until(self, timestamp):
        """Read event from aedat4 file until the given timestamp."""
        events = self.__read_raw_events_until(timestamp)
        self.filter_chain.accept(events)
        return self.filter_chain.generateEvents()

    def generate_frame(self, timestamp):
        """Generate an image frame at the given timestamp."""
        raise NotImplementedError


class AedatProcessorLinear(AedatProcessorBase):
    """Aedat file processor using accumulator with linear decay."""

    def __init__(self, path, contribution, decay, neutral_val, ignore_polarity=False, filter_noise=False):
        """
        Constructor.

        :param path: path to an aedat4 file to read
        :param contribution: event contribution
        :param decay: accumulator decay (linear)
        :param neutral_val:
        :param ignore_polarity:
        :param filter_noise: if true, noise pixels will be filtered out
        """
        super().__init__(path, filter_noise)

        # Accumulator drawing the events on images
        self.accumulator = dv.Accumulator(self.recording.getEventResolution(),
                                          decayFunction=dv.Accumulator.Decay.LINEAR,
                                          decayParam=decay,
                                          synchronousDecay=True,
                                          eventContribution=contribution,
                                          maxPotential=1.0,
                                          neutralPotential=neutral_val,
                                          minPotential=0.0,
                                          rectifyPolarity=ignore_polarity)

    def generate_frame(self, timestamp) -> np.ndarray:
        events = self.read_events_until(timestamp)
        self.accumulator.accept(events)

        image = self.accumulator.generateFrame().image
        assert image.dtype == np.uint8
        return image


def read_csv(path, is_with_ellipsis, is_with_coords):
    """
    Read a csv file and reatain all columns with the listed column names.
    Depending on the configuation, a different set of columns from the file is retained
    """
    header_items = ['timestamp', 'possible']
    if is_with_coords is True:
        header_items.append('center_x')
        header_items.append('center_y')
    if is_with_ellipsis is True:
        header_items.append('axis_x')
        header_items.append('axis_y')
        header_items.append('angle')

    label_file_df = pd.read_csv(path)
    label_file_df = label_file_df[header_items]

    return label_file_df


if __name__ == '__main__':
    # Example how to generate an image at the given timestamp,

    aedat_path = pathlib.Path("/home/username/Desktop/eye_tracking/data/EyeTrackingDataSet_FromInivation/2022-04-12_adam_01/events.aedat4")
    labels_path = pathlib.Path("/home/username/Desktop/eye_tracking/data/EyeTrackingDataSet_FromInivation/2022-04-12_adam_01/annotations.csv")

    aedat_processor = AedatProcessorLinear(aedat_path, 0.25, 1e-7, 0.5)
    labels = read_csv(labels_path, False, True)

    skip_cnt = 123
    for label in labels.itertuples():
        if skip_cnt > 0:
            skip_cnt -= 1
            continue

        ts = label.timestamp

        img = aedat_processor.generate_frame(ts)
        
        center_x = int(label.center_x)
        center_y = int(label.center_y)

        img = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
        img = cv2.circle(img, (center_x, center_y), radius=10, color=(255, 0, 0), thickness=-1)

        cv2.imshow("preview", img)
        cv2.waitKey(0)
        break

    # datasets.load_dataset

