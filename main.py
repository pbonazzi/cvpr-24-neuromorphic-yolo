import os, random, wandb, cv2, math, time
import numpy as np
from tqdm.auto import tqdm

# tensor
import torch
from torch.utils.data import DataLoader

# synsense
from sinabs.from_torch import from_model
from sinabs.backend.dynapcnn import DynapcnnNetwork

# custom
from data.dataset import IniVationDataset
from nets import get_summary
from nets.synsense import SynSenseEyeTracking
from loss import YoloLoss
from utils import draw_image

class arguments():
    def __init__(self):
        self.device = device
        self.SxS_Grid = 5
        self.num_boxes = 2
        self.num_classes = 1
        self.dsize = [128, 128]
        self.act_mode_8bit = None

def prepare(batch_size):
    global device, train_dataloader, test_dataloader, spiking_test_dataloader, input_image_size, args, criterion

    # Torch device
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    # Model folder to save trained models
    os.makedirs("models", exist_ok=True)

    # Setting up random seed to reproduce experiments
    torch.manual_seed(0)
    if device != "cpu":
        torch.backends.cudnn.deterministic = True
        torch.backends.cudnn.benchmark = False

    # Downloading/Loading MNIST dataset as tensors for training
    args = arguments()
    criterion = YoloLoss(args)
    base_path = "/home/thor/projects/data/EyeTrackingDataSet_FromInivation"
    list_experiments = random.sample(range(0, 10), 10) 
    train_dataset = IniVationDataset(base_path, device=device, args=args, list_experiments=list_experiments[2:], train=True) # leave two participants out
    test_dataset = IniVationDataset(base_path, device=device, args=args,  list_experiments=list_experiments[:2], train=False) 

    # Define Torch dataloaders for training, testing and spiking testing
    train_dataloader = DataLoader(train_dataset, batch_size=batch_size, shuffle=True)
    test_dataloader = DataLoader(test_dataset, batch_size=1, shuffle=True)
    spiking_test_dataloader = DataLoader(test_dataset, batch_size=1, shuffle=True)

    # Define the size of input images
    input_image_size = (1, args.dsize[0], args.dsize[1])

    # Return global prameters
    return (
        device,
        train_dataloader,
        test_dataloader,
        spiking_test_dataloader,
        input_image_size,
    )

def train(model, learning_rate=1e-3, n_epochs=20):

    # Define optimizer
    optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)
    #scheduler =  torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, 'min')

    # Visualize and display training loss in a progress bar
    pbar = tqdm(range(n_epochs), desc= "Training")

    # backprop over epochs
    for epoch in pbar:
        # over batches
        model.train()
        for num, (imgs, labels) in enumerate(train_dataloader):
            # reset grad to zero for each batch
            optimizer.zero_grad()
            # port to device
            imgs, labels = imgs.to(device), labels.to(device)
            # forward pass
            outputs = model(imgs)
            # calculate loss
            loss_dict, log_data = criterion(outputs, labels)

            # display loss in progress bar
            pbar.set_postfix(loss=loss_dict["loss"].item())

            # backward pass
            loss_dict["loss"].backward()
            torch.nn.utils.clip_grad_norm_(model.parameters(), 2)
            
            # optimze parameters
            optimizer.step()
        
        #scheduler.step()

        # loggings
        target_box = labels[..., 2:6][0].detach()
        pred_box = log_data["box_pred"][0].detach()
        point_target = (target_box[..., :2] + (target_box[..., 2:] - target_box[..., :2])/2).sum(0).sum(0) 
        point_pred = (pred_box[..., :2] + (pred_box[..., 2:] - pred_box[..., :2])/2).sum(0).sum(0) 
        accuracy = math.sqrt(((point_pred[0]-point_target[0])*args.dsize[0])**2 + ((point_pred[1]-point_target[1])*args.dsize[1])**2)
        wandb.log({f"train/accuracy": accuracy})
        log_img = draw_image(imgs[0], pred_box, target_box, point_pred, point_target)
        wandb.log({"train/images": wandb.Image(log_img.rotate(90))})
        for key in loss_dict.keys():
            wandb.log({f"train/{key}": loss_dict[key].detach()/batch_size})

        if epoch % 5 == 0:
            test(model, epoch)
            model.train()
            
    return model


def test(model, epoch):
    # Test the accuracy of a CNN model

    # With no gradient means less memory and calculation on forward pass
    with torch.no_grad():
        # evaluation usese Dropout and BatchNorm in inference mode
        model.eval()

        # Get the size of the first image
        width, height = 640, 480

        # Define the codec and create a VideoWriter object
        output_path = f"{epoch}.mp4"
        fps=5
        fourcc = cv2.VideoWriter_fourcc(*"mp4v")
        video_writer = cv2.VideoWriter(output_path, fourcc, fps, (width, height))

        # over batches
        exec_time, accuracy = 0, 0
        for imgs, labels in test_dataloader:

            # port to device
            imgs, labels = imgs.to(device), labels.to(device)

            start = time.time()
            outputs = model(imgs)
            end = time.time() 
            exec_time += ((end-start)*1000)

            # calculate loss
            loss_dict, log_data = criterion(outputs, labels)

            # epoch images
            target_box = labels[..., 2:6][0].detach()
            pred_box = log_data["box_pred"][0].detach()
            
            # accuracy
            point_target = (target_box[..., :2] + (target_box[..., 2:] - target_box[..., :2])/2).sum(0).sum(0) 
            point_pred = (pred_box[..., :2] + (pred_box[..., 2:] - pred_box[..., :2])/2).sum(0).sum(0) 
            accuracy += math.sqrt(((point_pred[0]-point_target[0])*args.dsize[0])**2 + ((point_pred[1]-point_target[1])*args.dsize[1])**2)
            
            log_img = draw_image(imgs[0], pred_box, target_box, point_pred, point_target)
            log_img.rotate(90)
            #wandb.log({"val/images": wandb.Image(log_img.rotate(90))})
            cv_image = cv2.cvtColor(np.array(log_img), cv2.COLOR_RGB2BGR)
            video_writer.write(cv_image) 


        video_writer.release()
        print("Video Released")


        # epoch loggings
        for key in loss_dict.keys():
            wandb.log({f"val/{key}": loss_dict[key].detach()/batch_size})

        wandb.log({f"val/accuracy": round(accuracy/len(test_dataloader), 2)})
        print(f"Accuracy {round(accuracy/len(test_dataloader), 2)} (px)")
        print(f"Latency {round(exec_time/len(test_dataloader), 2)} (ms)")


# Define tensor_tile function to generate sequence of input images
def tensor_tile(a, dim, n_tile):
    # a: input tensor
    # dim: tile on a specific dim or dims in a tuple
    # n_tile: number of tile to repeat
    init_dim = a.size(dim)
    repeat_idx = [1] * a.dim()
    repeat_idx[dim] = n_tile
    a = a.repeat(*(repeat_idx))
    order_index = torch.LongTensor(
        np.concatenate([init_dim * np.arange(n_tile) + i for i in range(init_dim)])
    )
    return torch.index_select(a, dim, order_index)


def snn_test(model, n_dt=10, n_test=10000):
    # Testing the accuracy of SNN on sinabs
    # model: CNN model
    # n_dt: the time window of each simulation
    # n_test: number of test images in total

    # Transfer Pytorch trained CNN model to sinabs SNN model
    net = from_model(
        model,  # Pytorch trained model
        input_image_size,  # Input image size: (n_channel, width, height)
        spike_threshold=1.0,  # Threshold of the membrane potential of a Spiking neuron
        bias_rescaling=1.0,  # Subtract membrane potential when the neuron fires a spike
        min_v_mem=-1.0,  # The lower bound of the membrane potential
        num_timesteps=n_dt, # The number of time steps
    ).to(device)

    # With no gradient means less memory and calculation on forward pass
    with torch.no_grad():
        # evaluation usese Dropout and BatchNorm in inference mode
        net.spiking_model.eval()
        # Count correct prediction and total test number
        n_correct = 0
        # loop over the input files once a time
        for i, (imgs, labels) in enumerate(tqdm(spiking_test_dataloader)):
            if i > n_test:
                break
            # tile image to a sequence of n_dt length as input to SNN
            input_frames = tensor_tile(imgs, 0, n_dt).to(device)
            labels = labels.to(device)
            # Reset neural states of all the neurons in the network for each inference
            net.reset_states()
            # inference
            outputs = net.spiking_model(input_frames)
    # calculate accuracy
    snn_accuracy = n_correct / n_test * 100.0
    print("SNN test accuracy: %.2f" % (snn_accuracy))
    return snn_accuracy


###################################
# bootstrap
num_epochs = 1000
learning_rate = 1e-4
batch_size = 64

wandb.init(
    project="sinabs",
    config={
    "learning_rate": learning_rate,
    "architecture": "BabyYolo",
    "dataset": "iniVation",
    "epochs": num_epochs,
    }
)

out_dir = os.path.join("output", "runs", wandb.run.name)
os.makedirs(out_dir, exist_ok=True)

# Setting up environment
prepare(batch_size=batch_size)

# Init SynSenseEyeTracking CNN
model = SynSenseEyeTracking(args).to(device)

# Check Memory // Device Compatibility
get_summary(model)

sinabs_model = from_model(model.seq, add_spiking_output=True,  synops=False,  batch_size=batch_size)

input_shape = (2, args.dsize[0], args.dsize[1])
dynapcnn_net = DynapcnnNetwork(
    sinabs_model.spiking_model.to("cpu"),
    input_shape=input_shape,
    discretize=True,
    dvs_input=True)
dynapcnn_net.to(device="dynapcnndevkit:0")


# Train CNN model
train(model, learning_rate=learning_rate, n_epochs=num_epochs)
test(model)