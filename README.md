# Neuromorphic YOLO

## Name
Neuromorphic YOLO: Real-Time Eye Tracking with Event Cameras on Neuromorphic and Accelerated Hardware

## Getting started

```
git clone https://gitlab.ethz.ch/pbonazzi/cvpr-24-neuromorphic-yolo.git
cd cvpr-24-neuromorphic-yolo
```

## Authors and acknowledgment
Pietro Bonazzi

## License
TBD

## Project status, Roadmap
CVPR 24
